function appendRules() {
    var htmlDiv = document.querySelector('footer');
    var customUI = htmlDiv.querySelector('div[class="customUI"]');
    var username = "";

    if (!customUI) {
        htmlDiv.innerHTML +=
        '<div class="customUI" style="display: block;">' +
            '<div style="bottom: 10px;left: 10px;right: 60%;min-width:500px;position: fixed;z-index: 150;background-color: #ffff;border: #878787 1px solid;border-radius: 12px;padding: 10px;">' +
                '<div style="border-bottom: 1px solid rgba(var(--b6a,219,219,219),1);flex-direction: row;height: 43px;margin-bottom:10px;"><div class="WaOAr"></div><h1 class="m82CD  TNiR1">Important : Instagram Rules</h1><div onclick="document.getElementsByClassName(\"customUI\")[0].style.display = "none";" class="WaOAr btnClose"><button class="wpO6b" type="button"><div class="QBdPU "><svg aria-label="Close" class="_8-yf5 " fill="#262626" height="24" viewBox="0 0 48 48" width="24"><path clip-rule="evenodd" d="M41.1 9.1l-15 15L41 39c.6.6.6 1.5 0 2.1s-1.5.6-2.1 0L24 26.1l-14.9 15c-.6.6-1.5.6-2.1 0-.6-.6-.6-1.5 0-2.1l14.9-15-15-15c-.6-.6-.6-1.5 0-2.1s1.5-.6 2.1 0l15 15 15-15c.6-.6 1.5-.6 2.1 0 .6.6.6 1.6 0 2.2z" fill-rule="evenodd"></path></svg></div></button></div></div>'+
                '<span style="font-size:16px;margin-left:5px;font-weight:bold;">- Follow/Unfollow limit per day:</span>' +
                '<span style="font-size:16px;margin-left:5px;">Starting at around 50 and progressively incressing every week to a maximum of 200. (+50 per week)</span>' +
                '<br><span style="font-size:16px;margin-left:5px;">(note: follow and unfollow count as the same action)</span><br>' +
                '<span style="font-size:16px;margin-left:5px;font-weight:bold;">- Danger:</span>' +
                '<span style="font-size:16px;margin-left:5px;">Be carefull when using this extension, bad parameters can definitively get you ban. If you\'re not sure about what you\'re doing, you should use the default configuration.</span>' +
                '<br><span style="font-size:16px;margin-left:5px;">(note: even the default config is not 100% safe)</span>' +
            '</div>' +
        '</div>';
    }

    document.querySelectorAll("script").forEach(script => {
        let objectBrut = script.innerText;

        if (objectBrut.includes("window._sharedData = ")) {
            objectBrut = objectBrut.split(/window._sharedData = |;/).join("");
            let object = JSON.parse(objectBrut);

            if (object.config && object.config.viewer && object.config.viewer.username) {
                username = object.config.viewer.username;
            }
        }
    });

    chrome.runtime.sendMessage({username: username}, (response) => {});

    delete window.htmlDiv;
    delete window.customUI;
    delete window.username;
}

appendRules();