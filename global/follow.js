async function getNames(startIndex, array) {
    let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
    let as = usersList.querySelectorAll('a[href]');
    let index = startIndex;

    for (index = 0; index < as.length; index++) {
        let title = as[index].getAttribute('title');

        if (as[index].getAttribute('href') && title) {
            if (array.indexOf(title) == -1) {
                array.push([title, as[index]]);
            }
        }
    }
    return index;
}

async function actionButton(textToClick) {
    let btns = document.querySelectorAll('button');

    btns.forEach(btn => {
        let innerText = btn.innerText;

        if (innerText == textToClick) {
            btn.click();
        }
    });
}

async function getFollowButton() {
    let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
    let btns = usersList.querySelectorAll('button');

    alreadyFollowed = [];
    for (let i = 0; i < btns.length; i++) {
        if (btns[i].innerText != options.follow_button_text) {
            alreadyFollowed.push(i);
        }
    }
    return Array.from(btns).filter(btn => {
        return btn.innerText == options.follow_button_text;
    });
}

async function likeSomePost(postToLike, windowToUse) {
    let post = null;
    let likeButton = null;
    let like = null;

    await sleep(1, 2);
    post = windowToUse.document.getElementsByClassName('v1Nh3');
    if (typeof post[postToLike] != 'undefined') {
        let link = null;
        let attempts = 0;

        for (attempts = 0; attempts < 5 && !link; attempts++) {
            console.log('Attempt to find link buttton. Try : ' + attempts);
            link = post[postToLike].firstChild;
            await sleep(getRandom(0.2, 0.5));
            if (link) {
                link.click();
                break;
            }
        }
        await sleep(getRandom(1, 2));
        for (attempts = 0; attempts < 5 && !likeButton; attempts++) {
            console.log('Attempt to find like buttton. Try : ' + attempts);
            likeButton = windowToUse.document.querySelectorAll('.wpO6b');
            await sleep(getRandom(0.2, 0.5));
            for (let i = 0; i < likeButton.length; i++) {
                like = likeButton[i].querySelector('svg[aria-label="' + options.like_button + '"]');
                if (like) {
                    likeButton[i].click()
                    await sleep(getRandom(1, 2));
                    windowToUse.history.back();
                    break;
                }
            }
        }
        if (!likeButton || !like) {
            windowToUse.history.back();
        }
        await sleep(getRandom(1, 2));
    }
}

async function followCycle() {
    let followButtons = await getFollowButton();
    let prevUsersListLength = -1;
    let usersListLength = 0;
    let followIndex = 0;
    let followNames = [];
    let skipName = 0;
    let followCount = 0;

    followSession = Math.round(getRandom(followCycleMin, followCycleMax));
    stopMethod = stopRunning;

    document.getElementById('statusCircle').style.backgroundColor = processingColor;
    document.getElementById('statusText').innerText = processingText + followCount + '/' + followSession;
    while (followCount < followSession) {
        let likeDone = false;

        if (followIndex != 0 && likeUser && Math.floor(getRandom(0, 10)) == 1) {
            console.log('Start liking : ' + likeCount + ' posts of : ' + followNames[followIndex + skipName][0]);
            followNames[followIndex + skipName][1].click();
            await sleep(getRandom(1, 2));
            for (let i = 0; i < likeCount; i++) {
                await likeSomePost(i, window);
                if (i == likeCount - 1) {
                    window.history.back();
                }
            }
            await sleep(getRandom(1, 2));
            document.querySelectorAll(".m82CD, .WaOAr").forEach((index) => {
                index.remove();
            });
            let statusParent = document.querySelector('.eiUFA');
        
            statusParent.innerHTML += statusPopup;
            document.getElementById('statusCircle').style.backgroundColor = processingColor;
            document.getElementById('statusText').innerText = processingText + followCount + '/' + followSession;
            likeDone = true;
        }
        if (likeDone || followIndex == 0 || !followButtons[followIndex]) {
            if (window.checkLikeOnUs !== undefined) {
                await checkLikeOnUs();
            }

            while (likeDone || (prevUsersListLength != usersListLength && followButtons.length < followSession - followCount + skipName)) {
                let parentDiv = document.querySelector('div[class="isgrP"]');
                let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
                let lis = usersList.querySelectorAll('li');

                prevUsersListLength = usersListLength;
                usersListLength = usersList.querySelectorAll('li').length;
                lis[lis.length - 1].click();
                parentDiv.scrollTo({
                    behavior: 'smooth',
                    top: usersList.clientHeight
                });
                await sleep(getRandom(0.5, 1));
                followButtons = await getFollowButton();
                likeDone = false;
            }
            skipName = 0;
            followIndex = 0;
            followNames = [];
            await getNames(0, followNames);
            if (followIndex >= followButtons.length) {
                return true;
            }
        }

        if (forceStop == true) {
            followSession = followCount;
            return true;
        }

        while (alreadyFollowed.indexOf(followIndex + skipName) != -1) {
            skipName++;
        }
        usersInfo.push(followNames[followIndex + skipName][0]);
        followButtons[followIndex].click();
        console.log('Follow : ' + followNames[followIndex + skipName][0]);
        setCookie('__Secure-action', parseInt(getCookie('__Secure-action'), 10) + 1, getCookie("__Secure-expire"));
        if (totalActions <= parseInt(getCookie('__Secure-action'), 10)) {
            return true;
        }
        followCount++;
        followIndex++;
        document.getElementById('statusCircle').style.backgroundColor = processingColor;
        document.getElementById('statusText').innerText = processingText + followCount + '/' + followSession;
        await sleep(getRandom(2, 5));
    }
    return false;
}

async function followEveryone() {
    let shouldStop = false;

    await sleep(getRandom(1, 3));
    if (!document.querySelector('ul[class="jSC57  _6xe7A"]')) {
        let as = document.querySelectorAll('a[href]');

        for (let i = 0; i < as.length; i++) {
            if (as[i].getAttribute('href').includes('/followers/')) {
                as[i].click();
                break;
            }
        }
    }
    await sleep(getRandom(1, 2));
    document.querySelectorAll(".m82CD, .WaOAr").forEach((index) => {
        index.remove();
    });

    let statusParent = document.querySelector('.eiUFA');
    statusParent.innerHTML += statusPopup;
    stopMethod = stopNotRunning;

    while (!shouldStop) {
        let date = 'Instagram' + getFormattedDate(getCookie("__Secure-expire"));
        let prevUsersInfo = (localStorage.getItem(date)) ? localStorage.getItem(date).split(',') : [];
        let sleepTime = 0;

        shouldStop = await followCycle();
        for (let i = 0; i < usersInfo.length; i++) {
            prevUsersInfo.push(usersInfo[i]);
        }
        localStorage.removeItem(date);
        localStorage.setItem(date, prevUsersInfo);
        usersInfo = [];
        await actionButton(options.unfollow_button_text);
        
        doRequest("POST", "/actions/ajaxSaveAction", {user_id: userId, action: "follow", count: followSession, service: "insta"}, () => {
            var data = JSON.parse(request.responseText);
            console.log(data);
        });

        stopMethod = stopNotRunning;
        if (!shouldStop) {
            console.log('Sleep...');
            document.getElementById('statusCircle').style.backgroundColor = sleepColor;
            sleepTime = Math.round(getRandom(sleepMin, sleepMax));

            while (sleepTime > 0) {
                if (sleepTime < 60) {
                    document.getElementById('statusText').innerText = sleepText + sleepTime + 's';
                } else {
                    document.getElementById('statusText').innerText = sleepText + Math.ceil(secToMin(sleepTime)) + 'min';
                }
                await sleep(1);
                sleepTime--;
            }
        }
        stopMethod = stopRunning;
    }
    console.log('Finish !');
    document.getElementById('statusCircle').style.backgroundColor = finishColor;
    document.getElementById('statusText').innerText = finishText;
    await sleep(1, 2);
    location.reload();
}

function doAsync() {
    if (!getCookie('__Secure-action')) {
        setActionCookie(1);
    }
    console.log('Actions today : ', getCookie('__Secure-action'));
    followEveryone();
}

function getFormattedDate(date) {
    let monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let splitedDate = date.split(/ /g);
    let month = (monthArray.indexOf(splitedDate[2]) + 1 >= 10) ? monthArray.indexOf(splitedDate[2]) + 1 : '0' + monthArray.indexOf(splitedDate[2]) + 1;

    return month + '/' + splitedDate[1] + '/' + splitedDate[3];
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, date) {
    let expires = "expires=" + date;

    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=instagram.com;secure;path=/";
}

function setActionCookie(exdays) {
    let date = new Date();

    date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
    if (getCookie("__Secure-expire") == "") {
        setCookie("__Secure-expire", date.toUTCString(), date.toUTCString());
    }
    setCookie("__Secure-action", 0, date.toUTCString());
}

function doRequest(method, URI, data, callback) {
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

function stopRunning() {
    if (document.getElementById('statusCircle')) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    forceStop = true;
}

function stopNotRunning() {
    if (document.getElementById('statusCircle')) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    location.reload();
}

if (once_global === undefined) {
    var once_global = true;
    document.querySelector("body").style += ";pointer-events: none;";

    var language = document.querySelector("html").getAttribute("lang");
    var GhfrLink = "https://www.instagram.com/growth_hacking_france/";
    var baseUrl = "https://extension-connect.isoluce.net";
    var request = new XMLHttpRequest();
    var userId = null;

    var minToSec = (min) => (min * 60);
    var secToMin = (sec) => (sec / 60);
    var getRandom = (min, max) => (Math.random() * (max - min) + min);
    var sleep = (seconds) => new Promise(_ => setTimeout(_, seconds * 1000));

    var forceStop = false;
    var stopMethod = stopNotRunning;
    var totalActions = null;
    var followSession = null;
    var followCycleMax = null;
    var followCycleMin = null;
    var sleepMin = null;
    var sleepMax = null;
    var likeUser = null;
    var startIndexFollow = 0;
    var usersInfo = [];
    var alreadyFollowed = [];
	var options = {
        // GOT FROM CHROME LOCAL STORAGE
		"total_actions": "",
		"follow_per_cycle_min": "",
		"follow_per_cycle_max": "",
		"follow_sleep_min": "",
		"follow_sleep_max": "",
		"like_user_count": "",
		"active_like": "",
		"unfollow_per_cycle": "",
		"mass_unfollow": "",
		"unfollow_sleep_min": "",
		"unfollow_sleep_max": "",
		"unfollow_everyone": "",
		"smart_unfollow": "",
		"days_before_unfollow": "",
        // DEFINED IN RUNTIME
		"follow_button_text": "",
		"following_button_text": "",
		"unfollow_button_text": "",
		"cancel_button_text" : ""
	};

	if (language == "fr") {
        options.follow_button_text = "S’abonner";
        options.following_button_text = "Abonné(e)";
        options.unfollow_button_text = "Se désabonner";
        options.like_button = "J’aime";
        options.cancel_button_text = "Annuler";
    }
	else if (language == "es" || language == "es-la") {
        options.follow_button_text = "Seguir";
        options.following_button_text = "Siguiendo";
        options.unfollow_button_text = "Dejar de seguir";
        options.like_button = "Me gusta"; 
        options.cancel_button_text = "Cancelar";
    }
    else {
        options.follow_button_text = "Follow";
        options.following_button_text = "Following";
        options.unfollow_button_text = "Unfollow";
        options.like_button = "Like";
        options.cancel_button_text = "Cancel";
    }

    var processingText = "Follow : ";
    var processingColor = "orange";

    var sleepText = "Sleep : ";
    var sleepColor = "red";

    var stoppingText = "Stopping...";
    var stoppingColor = "red";

    var finishText = "Finish !";
    var finishColor = "green";

    var statusPopup =
    '<style>\
        .margin-top{margin-top:6px;}.margin-left{margin-left:10px;}.status_text{font-weight: bold;font-size: 16px;display: inline-block;text-align: center;}.circle{display:inline-block;width:18px;height:18px;border-radius:18px;vertical-align:baseline;vertical-align:baseline;}\
    </style>\
    <div class="statusPopup" style="display: block; margin: auto;">\
        <nobr><p id="statusCircle" class="circle margin-left margin-top" style="background-color:' + processingColor + '"></p><p id="statusText" class="status_text margin-left margin-top">' + processingText + '</p></nobr>\
    </div>';

    setInterval(() => {
        let checking = setInterval(() => {
            chrome.runtime.sendMessage({directive: "throttling"});
        }, 500);

        requestAnimationFrame(() => {
            clearInterval(checking);
        });
    }, 100);

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.stop === undefined && request.isRunning === undefined) {
            return;
        }
        if (request.stop) {
            stopMethod();
            sendResponse({});
            return;
        }
        sendResponse({running: true});
    });
}