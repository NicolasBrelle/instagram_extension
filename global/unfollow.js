async function getNames(startIndex, array) {
    let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
    let as = usersList.querySelectorAll('a[href]');
    let index = startIndex;

    for (index = 0; index < as.length; index++) {
        let title = as[index].getAttribute('title');

        if (as[index].getAttribute('href') && title) {
            if (array.indexOf(title) == -1) {
                array.push(title);
            }
        }
    }
    return index;
}

async function actionUnfollow(textToClick) {
    let btns = document.querySelectorAll('button');

    for (let i = 0; i < btns.length; i++) {
        let innerText = btns[i].innerText;

        if (innerText == textToClick) {
            btns[i].click();
            await sleep(getRandom(0.5, 0.8));
        }
    }
}

async function getFollowers() {
    let parentDiv = document.querySelector('div[class="isgrP"]');
    let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
    let prevUsersListLength = -1;
    let usersListLength = 0;
    let startIndex = 0;

    if (!usersList) {
        let as = document.querySelectorAll('a[href]');

        if (as.length == 0) {
            return;
        }
        for (let i = 0; i < as.length; i++) {
            if (as[i].getAttribute('href').includes('/followers/')) {
                as[i].click();
                break;
            }
        }
        await sleep(2);
        document.querySelectorAll(".m82CD, .WaOAr").forEach((index) => {
            index.remove();
        });

        let statusParent = document.querySelector('.eiUFA');
        statusParent.innerHTML += statusPopup;
        stopMethod = stopNotRunning;

        usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
        parentDiv = document.querySelector('div[class="isgrP"]');
    }
    while (prevUsersListLength != usersListLength) {
        let lis = usersList.querySelectorAll('li');

        prevUsersListLength = usersListLength;
        startIndex = await getNames(startIndex, ignoreList);
        usersListLength = usersList.querySelectorAll('li').length;
        parentDiv.scrollTo({
            top: usersList.clientHeight,
            behavior: 'smooth'
        });
        await sleep(getRandom(0.5, 0.8));
        lis[usersListLength - 1].querySelector('button').click();
        await sleep(getRandom(0.5, 0.8));
    }
    await actionUnfollow(options.cancel_button_text);
    window.history.back();
    await sleep(getRandom(0.5, 0.8));
}

async function getIgnoreByScript() {
	var usersInfo = getAllStorage();
	var usersDate = Object.keys(usersInfo);

	if (usersDate.length > 0) {
		var dateDaysAgo = new Date();

		dateDaysAgo.setDate(dateDaysAgo.getDate() - daysBeforeUnfollow);
		dateDaysAgo = getFormattedDate(dateDaysAgo.toUTCString());
		dateDaysAgo = dateToInt(dateDaysAgo);

		for (var i = 0; i < usersDate.length; i++) {
			if (dateToInt(usersDate[i]) > dateDaysAgo) {
				var splitedUsers = usersInfo[usersDate[i]].split(',');

				for (var j = 0; j < splitedUsers.length; j++) {
					if (splitedUsers[j][0] == '@') {
						splitedUsers[j] = splitedUsers[j].substring(1, splitedUsers[j].length)
					}
					ignoreList.push(splitedUsers[j].toLowerCase());
				}
			}
            else {
				localStorage.removeItem(usersDate[i]);
			}
		}
    }
}

async function getUnfollowButton() {
    let usersList = document.querySelector('ul.jSC57._6xe7A');
    let btns = usersList.querySelectorAll('button');

    alreadyUnfollowed = [];
    for (let i = 0; i < btns.length; i++) {
        if (btns[i].innerText != options.following_button_text) {
            alreadyUnfollowed.push(i);
        }
    }
    return Array.from(btns).filter(btn => {
        return btn.innerText == options.following_button_text;
    });
}

async function unfollowCycle() {
    let unfollowSession = unfollowPerCycle;
    let unfollowButtons = await getUnfollowButton();
    let prevUsersListLength = -1;
    let usersListLength = 0;
    let unfollowIndex = 0;
    let followingNames = [];
    let skipName = 0;
    let lis = [];
    let unfollowBeforeClear = 0;
    let unfollowCount = 0;

    document.getElementById('statusCircle').style.backgroundColor = processingColor;
    document.getElementById('statusText').innerText = processingText + unfollowCount + '/' + unfollowSession;

    while (unfollowCount < unfollowSession) {
        if (unfollowIndex == 0 || !unfollowButtons[unfollowIndex]) {

            while (prevUsersListLength != usersListLength) {
                let parentDiv = document.querySelector('div[class="isgrP"]');
                let usersList = document.querySelector('ul[class="jSC57  _6xe7A"]');
                lis = usersList.querySelectorAll('li');

                prevUsersListLength = usersListLength;
                usersListLength = usersList.querySelectorAll('li').length;
                lis[lis.length - 1].click();
                parentDiv.scrollTo({
                    top: usersList.clientHeight,
                    behavior: 'smooth'
                });
                await sleep(getRandom(0.5, 0.8));
                unfollowButtons = await getUnfollowButton();
            }
            await getNames(0, followingNames);
            if (unfollowIndex >= unfollowButtons.length) {
                return true;
            }
        }

        if (forceStop == true) {
            unfollowPerCycle = unfollowCount;
            return true;
        }

        let shouldUnfollow = true;

        while (alreadyUnfollowed.indexOf(unfollowIndex + skipName) != -1) {
            skipName++;
        }
        if (!forceUnfollow) {
            let spans = lis[unfollowIndex + skipName].querySelectorAll('span');

            for (let i = 0; i < spans.length; i++) {
                if (spans[i].getAttribute('class') && spans[i].getAttribute('class').includes('coreSpriteVerifiedBadge')) {
                    shouldUnfollow = false;
                    console.log('Certified account.');
                    break;
                }
            }
        }
        for (let i = 0; shouldUnfollow == true && i < ignoreList.length && !forceUnfollow; i++) {
            if (ignoreList[i] == followingNames[unfollowIndex + skipName]) {
                shouldUnfollow = false;
                break;
            }
        }
        console.log('shouldUnfollow : ' + shouldUnfollow);
        if (shouldUnfollow) {
            unfollowButtons[unfollowIndex].scrollIntoView({
                behavior: 'smooth',
                block: 'start',
                inline: 'nearest'
            });
            unfollowButtons[unfollowIndex].click();
            setCookie('__Secure-action', parseInt(getCookie('__Secure-action'), 10) + 1, getCookie("__Secure-expire"));
            if (totalActions <= parseInt(getCookie('__Secure-action'), 10)) {
                return true;
            }
            unfollowCount++;
            document.getElementById('statusCircle').style.backgroundColor = processingColor;
            document.getElementById('statusText').innerText = processingText + unfollowCount + '/' + unfollowSession;
            if (++unfollowBeforeClear % Math.floor(getRandom(2, 4)) == 0) {
                await actionUnfollow(options.unfollow_button_text);
            }
            await sleep(getRandom(1, 2));
        }
        unfollowIndex++;
    }
    await actionUnfollow(options.unfollow_button_text);
    return false;
}

async function unfollowEveryone() {
    let shouldStop = false;
    let as = null;

    if (document.querySelector('ul[class="jSC57  _6xe7A"]')) {
        window.history.back();
        await sleep(getRandom(0.6, 1));
    }
    if (!forceUnfollow) {
        await getFollowers();
    }
    if (smartUnfollow) {
        await getIgnoreByScript();
    }

    await sleep(getRandom(1, 3));
    as = document.querySelectorAll('a[href]');
    for (let i = 0; i < as.length; i++) {
        if (as[i].getAttribute('href').includes('/following/')) {
            as[i].click();
            break;
        }
    }
    await sleep(getRandom(1, 2));
    document.querySelectorAll(".m82CD, .WaOAr").forEach((index) => {
        index.remove();
    });
    let statusParent = document.querySelector('.eiUFA');
    statusParent.innerHTML += statusPopup;

    while (!shouldStop) {
        stopMethod = stopRunning;

        shouldStop = await unfollowCycle();

        doRequest("POST", "/actions/ajaxSaveAction", {user_id: userId, action: "unfollow", count: unfollowPerCycle, service: "insta"}, () => {
            var data = JSON.parse(request.responseText);
            console.log(data);
        });

        stopMethod = stopNotRunning;

        if (!shouldStop && !massUnfollow) {
            console.log('Sleep...');
            document.getElementById('statusCircle').style.backgroundColor = sleepColor;
            sleepTime = Math.ceil(getRandom(sleepMin, sleepMax));

            while (sleepTime > 0) {
                if (sleepTime < 60) {
                    document.getElementById('statusText').innerText = sleepText + sleepTime + 's';
                }
                else {
                    document.getElementById('statusText').innerText = sleepText + Math.ceil(secToMin(sleepTime)) + 'min';
                }
                await sleep(1);
                sleepTime--;
            }
        }
        stopMethod = stopRunning;
    }
    console.log('Finish !');
    document.getElementById('statusCircle').style.backgroundColor = finishColor;
    document.getElementById('statusText').innerText = finishText;
    await sleep(1, 2);
    location.reload();
}

function doAsync() {
    if (!getCookie('__Secure-action')) {
        setActionCookie(1);
    }
    console.log('Actions today : ', getCookie('__Secure-action'));
    unfollowEveryone();
}

function getFormattedDate(date) {
    let monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let splitedDate = date.split(/ /g);
    let month = (monthArray.indexOf(splitedDate[2]) + 1 >= 10) ? monthArray.indexOf(splitedDate[2]) + 1 : '0' + monthArray.indexOf(splitedDate[2]) + 1;

    return month + '/' + splitedDate[1] + '/' + splitedDate[3];
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, date) {
    let expires = "expires=" + date;

    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=instagram.com;secure;path=/";
}

function setActionCookie(exdays) {
    let date = new Date();

    date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
    if (getCookie("__Secure-expire") == "") {
        setCookie("__Secure-expire", date.toUTCString(), date.toUTCString());
    }
    setCookie("__Secure-action", 0, date.toUTCString());
}

function dateToInt(date) {
    let splitedDate = date.split('/');

    return splitedDate[2] + splitedDate[0] + splitedDate[1];
}

function getAllStorage() {
    let values = {};
    let keys = Object.keys(localStorage);
    let i = keys.length;
    let isDate = new RegExp("Instagram../../....");

    while (i--) {
        if (isDate.test(keys[i])) {
            values[keys[i].replace('Instagram', '')] = localStorage.getItem(keys[i]);
        }
    }
    return values;
}

function doRequest(method, URI, data, callback) {
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

function stopRunning() {
    if (document.getElementById('statusCircle') !== null || document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    forceStop = true;
}

function stopNotRunning() {
    if (document.getElementById('statusCircle') !== null || document.getElementById('statusCircle').length != 0) {
        document.getElementById('statusCircle').style.backgroundColor = stoppingColor;
        document.getElementById('statusText').innerText = stoppingText;
    }
    location.reload();
}

if (once_global === undefined) {
    var once_global = true;
    document.querySelector("body").style += ";pointer-events: none;";

    var language = document.querySelector("html").getAttribute("lang");
    var baseUrl = "https://extension-connect.isoluce.net";
    var request = new XMLHttpRequest();
    var userId = null;

    var minToSec = (min) => (min * 60);
    var secToMin = (sec) => (sec / 60);
    var getRandom = (min, max) => (Math.random() * (max - min) + min);
    var sleep = (seconds) => new Promise(_ => setTimeout(_, seconds * 1000));

    var forceStop = false;
    var stopMethod = stopNotRunning;
    var totalActions = null;
    var unfollowPerCycle = null;
    var massUnfollow = null;
    var sleepMin = null;
    var sleepMax = null;
    var forceUnfollow = null;
    var smartUnfollow = null;
    var daysBeforeUnfollow = null;
    var ignoreList = [];
    var alreadyUnfollowed = [];
	var options = {
        // GOT FROM CHROME LOCAL STORAGE
		"total_actions": "",
		"follow_per_cycle_min": "",
		"follow_per_cycle_max": "",
		"follow_sleep_min": "",
		"follow_sleep_max": "",
		"like_user_count": "",
		"active_like": "",
		"unfollow_per_cycle": "",
		"mass_unfollow": "",
		"unfollow_sleep_min": "",
		"unfollow_sleep_max": "",
		"unfollow_everyone": "",
		"smart_unfollow": "",
		"days_before_unfollow": "",
        // DEFINED IN RUNTIME
		"follow_button_text": "",
		"following_button_text": "",
		"unfollow_button_text": "",
		"cancel_button_text" : ""
	};

	if (language == "fr") {
        options.follow_button_text = "S’abonner";
        options.following_button_text = "Abonné(e)";
        options.unfollow_button_text = "Se désabonner";
        options.like_button = "J’aime";
        options.cancel_button_text = "Annuler";
    }
    else if (language == "es" || language == "es-la") {
        options.follow_button_text = "Seguir";
        options.following_button_text = "Siguiendo";
        options.unfollow_button_text = "Dejar de seguir";
        options.like_button = "Me gusta"; 
        options.cancel_button_text = "Cancelar";
    }
    else {
        options.follow_button_text = "Follow";
        options.following_button_text = "Following";
        options.unfollow_button_text = "Unfollow";
        options.like_button = "Like";
        options.cancel_button_text = "Cancel";
    }

    var initText = "Setup...";
    var initColor = "grey";

    var processingText = "Unfollow : ";
    var processingColor = "orange";

    var sleepText = "Sleep : ";
    var sleepColor = "red";

    var stoppingText = "Stopping...";
    var stoppingColor = "red";

    var finishText = "Finish !";
    var finishColor = "green";

    var statusPopup =
    '<style>\
        .margin-top{margin-top:6px;}.margin-left{margin-left:10px;}.status_text{font-weight: bold;font-size: 16px;display: inline-block;text-align: center;}.circle{display:inline-block;width:18px;height:18px;border-radius:18px;vertical-align:baseline;vertical-align:baseline;}\
    </style>\
    <div class="statusPopup" style="display: block; margin: auto;">\
        <nobr><p id="statusCircle" class="circle margin-left margin-top" style="background-color:' + initColor + '"></p><p id="statusText" class="status_text margin-left margin-top">' + initText + '</p></nobr>\
    </div>';

    setInterval(() => {
        let checking = setInterval(() => {
            chrome.runtime.sendMessage({directive: "throttling"});
        }, 500);

        requestAnimationFrame(() => {
            clearInterval(checking);
        });
    }, 100);

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.stop === undefined && request.isRunning === undefined) {
            return;
        }
        if (request.stop) {
            stopMethod();
            sendResponse({});
            return;
        }
        sendResponse({running: true});
    });
}