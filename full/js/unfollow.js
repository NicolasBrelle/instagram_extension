if (typeof once == "undefined") {
    var once = true;

	// GET OPTIONS FROM CHROME LOCAL STORAGE
	chrome.storage.local.get("inputsValue", function(obj) {
		if (obj.inputsValue) {
			let optionsKeys = Object.keys(options);
			let values = obj.inputsValue;

			for (let i = 0; i < values.length; i++) {
				options[optionsKeys[i]] = values[i];
			}
		}

        totalActions = parseInt(options.total_actions, 10);
        unfollowPerCycle = parseInt(options.unfollow_per_cycle, 10);
        massUnfollow = options.mass_unfollow;
        sleepMin = minToSec(parseInt(options.unfollow_sleep_min, 10));
        sleepMax = minToSec(parseInt(options.unfollow_sleep_max, 10));
        forceUnfollow = options.unfollow_everyone;
        smartUnfollow = options.smart_unfollow;
        daysBeforeUnfollow = parseInt(options.days_before_unfollow, 10);

        chrome.storage.local.get("userId", function(obj) {
            userId = parseInt(obj.userId, 10);
            doAsync();
        });
	});
}