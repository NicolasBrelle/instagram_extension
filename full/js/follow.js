if (typeof once == "undefined") {
    var once = true;

	// GET OPTIONS FROM CHROME LOCAL STORAGE
	chrome.storage.local.get("inputsValue", function(obj) {
		if (obj.inputsValue) {
			let optionsKeys = Object.keys(options);
			let values = obj.inputsValue;

			for (let i = 0; i < values.length; i++) {
				options[optionsKeys[i]] = values[i];
			}
		}

        totalActions = parseInt(options.total_actions, 10);
        followCycleMax = parseInt(options.follow_per_cycle_max, 10);
        followCycleMin = parseInt(options.follow_per_cycle_min, 10);
        sleepMin = minToSec(parseInt(options.follow_sleep_min, 10));
        sleepMax = minToSec(parseInt(options.follow_sleep_max, 10));
        likeCount = parseInt(options.like_user_count, 10);
        likeUser = !options.active_like;
        
        chrome.storage.local.get("userId", function(obj) {
            userId = parseInt(obj.userId, 10);
            doAsync();
        });
    });
}