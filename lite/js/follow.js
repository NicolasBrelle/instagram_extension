if (typeof once == "undefined") {
    var once = true;

    // LITE SPECIFIC FUNCTION
    var checkLikeOnUs = async function () {
        let actionCount = parseInt(getCookie('__Secure-action'), 10);

        if (actionCount % 24 == 0 && actionCount != 0) {
            let tempWindow = window.open(GhfrLink, 'likeSomePost');

            window.focus();
            await likeSomePost(0, tempWindow);
            tempWindow.close();
            await sleep(getRandom(1, 2));
        }
    }

	// GET OPTIONS FROM CHROME LOCAL STORAGE
	chrome.storage.local.get("inputsValue", function(obj) {
		if (obj.inputsValue) {
			let optionsKeys = Object.keys(options);
			let values = obj.inputsValue;

			for (let i = 0; i < values.length; i++) {
				options[optionsKeys[i]] = values[i];
			}
		}

        totalActions = parseInt(options.total_actions, 10);
        followCycleMax = parseInt(options.follow_per_cycle_max, 10);
        followCycleMin = parseInt(options.follow_per_cycle_min, 10);
        sleepMin = minToSec(parseInt(options.follow_sleep_min, 10));
        sleepMax = minToSec(parseInt(options.follow_sleep_max, 10));
        likeCount = parseInt(options.like_user_count, 10);
        likeUser = !options.active_like;

        chrome.storage.local.get("userId", function(obj) {
            userId = parseInt(obj.userId, 10);
            doAsync();
        });
    });
}