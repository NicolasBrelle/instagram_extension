var request = new XMLHttpRequest();
const baseUrl = "https://extension-connect.isoluce.net";

function doRequest(method, URI, data, callback) {
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.directive === undefined) {
        return;
    }

    // CHECK FOR WICH UNFOLLOW JS TO START
    if (request.directive.includes("unfollow")) {
        chrome.tabs.executeScript(null, {
            file: "global/unfollow.js",
            allFrames: false
        });

        if (request.directive == "unfollow_full") {
            chrome.tabs.executeScript(null, {
                file: "full/js/unfollow.js",
                allFrames: false
            });
            sendResponse({});
            return;
        }

        chrome.tabs.executeScript(null, {
            file: "lite/js/unfollow.js",
            allFrames: false
        });
        sendResponse({});
    }

    // CHECK FOR WICH FOLLOW JS TO START
    if (request.directive.includes("follow")) {
        chrome.tabs.executeScript(null, {
            file: "global/follow.js",
            allFrames: false
        });

        if (request.directive == "follow_full") {
            chrome.tabs.executeScript(null, {
                file: "full/js/follow.js",
                allFrames: false
            });
            sendResponse({});
            return;
        }

        chrome.tabs.executeScript(null, {
            file: "lite/js/follow.js",
            allFrames: false
        });
        sendResponse({});
    }

    if (request.directive == "get_stats") {
        chrome.tabs.executeScript(null, {
            file: "stats.js",
            allFrames: false
        });
        sendResponse({});
    }

    if (request.directive == "throttling") {
        let tabId = sender.tab.id;
        let windowId = sender.tab.windowId;

        chrome.windows.update(windowId, {focused: true});
        chrome.tabs.update(tabId, {'active': true});

        doRequest("POST", "/users/ajaxSaveLog", {service: "instagram", message: "Throttling !"}, () => {});
        sendResponse({});
    }
});

chrome.runtime.onConnect.addListener(function (externalPort) {
    chrome.tabs.executeScript(null, {
        file: "rules.js",
        allFrames: false
    });
});